#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>

void remove_folder(char *foldername)
{
    pid_t pid = fork();
    if (pid == -1)
    {
        perror("Failed to fork");
        exit(1);
    }
    else if (pid == 0)
    {
        // child process
        char command[100];
        sprintf(command, "rm -rf %s", foldername);
        execlp("sh", "sh", "-c", command, NULL);
        exit(0);
    }
}

void zip_folder(char *foldername)
{
    pid_t pid = fork();
    if (pid == -1)
    {
        perror("Failed to fork");
        exit(1);
    }
    else if (pid == 0)
    {
        // child process
        char command[100];
        sprintf(command, "zip -rq %s.zip %s", foldername, foldername);
        execlp("sh", "sh", "-c", command, NULL);
        exit(0);
    }
    else if (pid > 0)
    {
        int zip_status;
        waitpid(pid, &zip_status, 0);
        remove_folder(foldername);
    }
}

void create_killer(char *program_name, int child_pid, int mode)
{
    FILE *fp;
    char killer_name[50];
    sprintf(killer_name, "killer");

    fp = fopen(killer_name, "w+");

    if (mode)
        fprintf(fp, "#!/bin/bash\nkill -9 %d\nrm $0\n", child_pid);
    else
    {
        fprintf(fp, "#!/bin/bash\n pkill -f \"%s -b\"\nrm $0\n", program_name);
    }

    fclose(fp);

    chmod(killer_name, 0777);
}

int main(int argc, char *argv[])
{

    pid_t pid,
        sid; // Variabel untuk menyimpan PID

    pid = fork(); // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
     * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
     * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);
    int i, j;
    int delay_folder = 30;  // penundaan pembuatan folder dalam detik
    int delay_download = 5; // penundaan download gambar dalam detik

    if (argc >= 2 && strcmp(argv[1], "-a"))
        create_killer(argv[0], (int)getpid(), 0);
    else
    {
        create_killer(argv[0], (int)getpid(), 1);
    }
    while (1)
    {
        pid_t child_id = fork();
        if (child_id == 0)
        {
            // download gambar
            char foldername[50];

            struct tm *timeinfo;
            time_t now = time(NULL);
            timeinfo = localtime(&now);
            strftime(foldername, 50, "%Y-%m-%d_%H:%M:%S", timeinfo);
            mkdir(foldername, 0777);

            for (j = 0; j < 15; j++)
            {
                char filename[50];
                struct tm *rawtime;
                time_t ftime = time(NULL);
                struct tm *timefile = localtime(&ftime);
                strftime(filename, 50, "%Y-%m-%d_%H:%M:%S", timefile);
                strcat(filename, ".jpg");

                // download gambar dari https://picsum.photos/
                int epoch;
                epoch = (int)time(NULL);
                char url[50];
                sprintf(url, "https://picsum.photos/%d", (epoch % 950) + 50);
                char command[100];
                sprintf(command, "./%s/%s", foldername, filename);

                pid_t pid;
                pid = fork();
                if (pid == -1)
                {
                    perror("Failed to fork");
                    exit(1);
                }
                else if (pid == 0)
                {
                    // child process
                    execlp("wget", "wget", "-O", command, "-q", url, NULL);
                    exit(0);
                }

                // tunggu 5 detik sebelum download gambar berikutnya
                sleep(delay_download);
            }
            zip_folder(foldername);
            exit(1);
        }
        sleep(delay_folder);
    }

    return 0;
}
