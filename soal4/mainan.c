#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>

void create_daemon() {
    pid_t pid = fork();

    if (pid < 0) exit(1);

    if (pid > 0) exit(0);

    umask(0);

    pid_t sid = setsid();

    if (sid < 0) exit(1);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

void run_script(char* script_path) {
    pid_t pid = fork();

    if (pid == 0) {
        int fd = open("/dev/null", O_RDWR);
        dup2(fd, STDIN_FILENO);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);
        close(fd);
        
        execl("/bin/bash", "bash", script_path, NULL);
        exit(1);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        exit(1);
    }
}

int is_time_valid(int hour, int minute, int second) {
    time_t current_time = time(NULL);
    struct tm *local_time = localtime(&current_time);

    return ((hour == -1 || local_time->tm_hour == hour) &&
        (minute == -1 || local_time->tm_min == minute) &&
        (second == -1 || local_time->tm_sec == second));
}

void run_loop(int hour, int minute, int second, char* script_path) {
    while (1) {
        if (is_time_valid(hour, minute, second)) {
            run_script(script_path);
        }
        sleep(1);
    }
}

int main(int argc, char *argv[]) {
    int err;
    if (argc != 5) {
        printf("Penggunaan: %s <Jam> <Menit> <Detik> <Path_file>\n", argv[0]);
        return 1;
    }

    int hour = (strcmp(argv[1], "*") == 0) ? -1 : atoi(argv[1]);
    int minute = (strcmp(argv[2], "*") == 0) ? -1 : atoi(argv[2]);
    int second = (strcmp(argv[3], "*") == 0) ? -1 : atoi(argv[3]);
    char *script_path = argv[4];

    if (hour < -1 || hour > 23) {
        printf("Error: Jam harus diantara 0-23 atau tanda *\n");
        err = 1;
    }
    if (minute < -1 || minute > 59) {
        printf("Error: Menit harus diantara 0-59 atau tanda *\n");
        err = 1;
    }
    if (second < -1 || second > 59) {
        printf("Error: Detik harus diantara 0-59 atau tanda *\n");
        err = 1;
    }
    if (access(script_path, F_OK) == -1) {
    printf("Error: File tidak ditemukan\n");
    err = 1;
    } else {
        // cek apakah file adalah file bash
        char *extension = strrchr(script_path, '.');
        if (extension == NULL || strcmp(extension, ".sh") != 0) {
            printf("Error: File harus berformat .sh\n");
            err = 1;
        }
    }

    if(err) return 1;

    create_daemon();

    run_loop(hour, minute, second, script_path);

    return 0;
}
