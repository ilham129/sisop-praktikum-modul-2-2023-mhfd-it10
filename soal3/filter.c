#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>

typedef struct {
    char name[20];
    char tim[20];
    char posisi[20];
    int ranking;
} Player;

void DirektoriPemain(const char *direktori, Player *pemain, int *num_pemain) {
    DIR *dir = opendir(direktori);
    if (dir == NULL) {
        printf("Tidak bisa membuka direktori %s\n", direktori);
        return;
    }
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        const char *ext = strrchr(entry->d_name, '.');
        if (ext != NULL && strcmp(ext, ".png") == 0) {
            char *name = strtok(entry->d_name, "_");
            char *tim = strtok(NULL, "_");
            char *posisi = strtok(NULL, "_");
            char *ranking_str = strtok(NULL, "_");
            if (name != NULL && tim != NULL && posisi != NULL && ranking_str != NULL) {
               
                int ranking = atoi(ranking_str);

                Player pemainn;
                strcpy(pemainn.name, name);
                strcpy(pemainn.tim, tim);
                strcpy(pemainn.posisi, posisi);
                pemainn.ranking = ranking;
                pemain[*num_pemain] = pemainn;
                (*num_pemain)++;
            } else {
                printf("Error parsing player from filename: %s\n", entry->d_name);
            }
        }
    }
    closedir(dir);
}

int BandingkanPemain(const void *a, const void *b) {
    Player *player1 = (Player *) a;
    Player *player2 = (Player *) b;

    int pos_cmp = strcmp(player1->posisi, player2->posisi);
    if (pos_cmp != 0) {
        return pos_cmp;
    }

    return player2->ranking - player1->ranking;
}

void BuatTim(int num_kiper, int num_bek, int num_gelandang, int num_penyerang) {
    Player pemain[100];
    int num_pemain = 0;

    DirektoriPemain("./players/players/Kiper", pemain, &num_pemain);
    DirektoriPemain("./players/players/Bek", pemain, &num_pemain);
    DirektoriPemain("./players/players/Gelandang", pemain, &num_pemain);
    DirektoriPemain("./players/players/Penyerang", pemain, &num_pemain);

    qsort(pemain, num_pemain, sizeof(Player), BandingkanPemain);

    int num_selected_kiper = 0;
    int num_selected_bek = 0;
    int num_selected_gelandang = 0;
    int num_selected_penyerang = 0;

    Player selectedPlayer[12];
    int selected_count=0;

    for (int i = 0; i < num_pemain && num_selected_kiper < num_kiper; i++) {
        if (strcmp(pemain[i].posisi, "Kiper") == 0) {
            printf(" %s (ranking: %d) posisi kiper \n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_kiper++;
        }
    }
    for (int i = 0; i < num_pemain && num_selected_bek < num_bek; i++) {
        if (strcmp(pemain[i].posisi, "Bek") == 0) {
            printf(" %s (ranking: %d) posisi bek\n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_bek++;
        }
    }
    for (int i = 0; i < num_pemain && num_selected_gelandang < num_gelandang; i++) {
        if (strcmp(pemain[i].posisi, "Gelandang") == 0) {
            printf(" %s (ranking: %d) posisi gelandang\n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_gelandang++;
        }
    }

    for (int i = 0; i < num_pemain && num_selected_penyerang < num_penyerang; i++) {
        if (strcmp(pemain[i].posisi, "Penyerang")== 0) {
            printf("%s (ranking: %d) posisi penyerang\n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_penyerang++;
        }
    }

if (num_selected_kiper < num_kiper) {
    printf("Error %d  %d \n", num_selected_kiper, num_kiper);
}
if (num_selected_bek < num_bek) {
    printf("Error %d %d \n", num_selected_bek, num_bek);
}
if (num_selected_gelandang < num_gelandang) {
    printf("Error %d %d \n", num_selected_gelandang, num_gelandang);
}
if (num_selected_penyerang < num_penyerang) {
    printf("Error %d %d \n", num_selected_penyerang, num_penyerang);
}

char filename[100];
sprintf(filename, "./users/formasi_%d_%d_%d.txt", num_bek, num_gelandang, num_penyerang);
FILE *file = fopen(filename, "w");
fprintf(file, "Formasi terbaik untuk %d-%d-%d:\n", num_bek, num_gelandang, num_penyerang);
fprintf(file, "-----------------------\n");
for (int i = 0; i < 11; i++) {
    fprintf(file, " %s Posisi: %s, name: %s, ranking: %d\n",
        "\n",selectedPlayer[i].posisi , selectedPlayer[i].name, selectedPlayer[i].ranking);
}
fclose(file);}

int main(void)
{
   
    pid_t  pid;
    int status;

    pid = fork();
    if (pid == 0) {
         execlp("wget", "wget","-O","players.zip","https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL);         exit(EXIT_SUCCESS);
     } else {
         waitpid(pid, &status, 0);
     }

    pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip","-oq", "players.zip","-d","players", NULL);
        exit(EXIT_SUCCESS);
    } else {
        waitpid(pid, &status, 0);
    }

    pid = fork();
    if (pid == 0) {
        execlp("rm", "rm", "players.zip", NULL);
        exit(EXIT_SUCCESS);
    } else {
        waitpid(pid, &status, 0);
    }

pid = fork();
if (pid == -1) {
    printf("Gagal membuat proses baru untuk menghapus file yang tidak mengandung 'ManUtd'.\n");
    return 1;
} else if (pid == 0) {
    if (execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "rm", "{}", "+", NULL) == -1) {
        printf("Gagal menjalankan perintah find.\n");
        return 1;
    }
} else {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
        printf("File yang tidak mengandung 'ManUtd' dihapus.\n");
    else
        printf("Gagal menghapus file yang tidak mengandung 'ManUtd'.\n");
}

pid = fork();
if (pid == 0) {
    execlp("mkdir", "mkdir", "./players/players/Penyerang", NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "-t", "./players/players/Penyerang", "{}", "+", (char *) NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("mkdir", "mkdir", "./players/players/Bek", NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "-name", "*Bek*", "-exec", "mv", "-t", "./players/players/Bek", "{}", "+", (char *) NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("mkdir", "mkdir", "./players/players/Gelandang", NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "-name", "*Gelandang*", "-exec", "mv", "-t", "./players/players/Gelandang", "{}", "+", (char *) NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("mkdir", "mkdir", "./players/players/Kiper", NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "-name", "*Kiper*", "-exec", "mv", "-t", "./players/players/Kiper", "{}", "+", (char *) NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

int num_bek, num_gelandang, num_penyerang;

printf("Bek: ");
scanf("%d", &num_bek);

printf("Gelandang: ");
scanf("%d", &num_gelandang);

printf("Penyerang: ");
scanf("%d", &num_penyerang);

BuatTim(1, num_bek, num_gelandang, num_penyerang);

    return 0;
}
