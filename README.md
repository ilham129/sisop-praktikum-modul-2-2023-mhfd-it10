# sisop-praktikum-modul-2-2023-MHFD-IT10

# Nomor 1
## Analisa Soal
Diminta untuk mendownload file zip dari link, melakukan unzip, pilih 1 file secara random, mengklasifikasikan ke folder sesuai perintah, mengzip tiap folder dan menghapus folder tersebut.

## Langkah Pengerjaan
Membuat kode sesuai permintaan soal, karena dilarang menggunakan system, maka digunakan exec dengan fork.
Untuk pick random, dilakukan dengan memasukkan semua file ke sebuah array, kemudian pilih acak dari array tersebut

## binatang.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <dirent.h>
#include <string.h>

void download_file() {
    pid_t pid = fork();

    if (pid == 0) { // child process
        char *args[] = {"wget", "-q", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        execvp("wget", args);
    } 
    else { // parent process
        wait(NULL);
        printf("Download selesai.\n");
    }
}

void unzip_file() {
    pid_t pid2 = fork();

    if (pid2 == 0) { // child process
        char *args[] = {"unzip", "-q", "binatang.zip", "-d", ".", NULL};
        execvp("unzip", args);
    } 
    else { // parent process
        wait(NULL);
        printf("Unzip selesai.\n");
    }
}

void pick_random() {
    DIR *dir;
    struct dirent *ent;
    char *folder = ".";
    int count = 0;
    char *files[100];

    dir = opendir(folder);

    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            char *filename = ent->d_name;
            if (strcmp(filename + strlen(filename) - 4, ".jpg") == 0) {
                files[count] = filename;
                count++;
            }
        }
    }

    closedir(dir);

    srand(time(NULL));
    int shuffle = rand() % count;

    printf("Shift Jaga: %s\n", files[shuffle]);
}

void group_files() {
    char *folders[] = {"HewanDarat", "HewanAir", "HewanAmphibi"};
    for (int i = 0; i < 3; i++) mkdir(folders[i], 0777);
    
    DIR *dir;
    struct dirent *ent;
    char *folder = ".";
    
    dir = opendir(folder);
    
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            char *filename = ent->d_name;
            char* file_extension = strrchr(filename, '.');
            if (file_extension && strcmp(file_extension, ".jpg") == 0) {
                char* animal_type = strrchr(filename, '_');
                char* dir_path;
                if (strcmp(animal_type + 1, "darat.jpg") == 0) {
                    dir_path = "HewanDarat";
                } 
                else if (strcmp(animal_type + 1, "amphibi.jpg") == 0) {
                    dir_path = "HewanAmphibi";
                } 
                else {
                    dir_path = "HewanAir";
                }
                char path[1000];
                sprintf(path, "%s/%s", dir_path, filename);
                rename(filename, path);
            }
        }
    }

    closedir(dir);
}

void zip_files() {
    int status;
    char *folders[] = {"HewanDarat", "HewanAir", "HewanAmphibi"};
    for (int i = 0; i < 3; i++) {
        pid_t pid = fork();

        if (pid == 0) {
            // Child process:
            char *args[] = {"zip", "-qr", folders[i], folders[i], NULL};
            execvp("zip", args);
        } else if (pid > 0) {
            // Parent process
            waitpid(pid, &status, 0);
        } else {
            // Error forking
            printf("Error forking\n");
            exit(1);
        }
    }
    printf("Zip selesai.\n");
    // Wait for all child processes to finish
    while (wait(&status) > 0);

    // Remove folders
    for (int i = 0; i < 3; i++) {
        pid_t pid = fork();

        if (pid == 0) {
            char* rm_args[] = {"rm", "-rf", folders[i], NULL};
            execvp("rm", rm_args);

            // If execvp() returns, there was an error
            perror("Error executing rm command");
            exit(EXIT_FAILURE);
        } 
        else if (pid < 0) {
            // Fork error
            perror("Error forking");
            exit(EXIT_FAILURE);
        }
    }

    // Wait for all child processes to finish
    while (wait(&status) > 0);
}

int main() {
    //A
    download_file();
    unzip_file();
    //B
    pick_random();
    //C
    group_files();
    //D
    zip_files();
    return 0;
}
```
## Test Output
![1](https://i.ibb.co/tzmPy2H/1.jpg)

## Kendala
Kendala pada saat zip kemudian remove, ditemukan solusi dengan menunggu zip selesai baru dilakukan remove

# Nomor 2
## Analisa Soal
Pada soal ini diminta agar dapat membuat program yang dimana bisa membuat folder 30 detik sekali dan mendownload 15 foto didalamnya dan setelah berisi 15 foto langsung meng-zip kemudian diminta juga untuk membuat program killer yang dimana program tersebut akan memberhentikan induk proses ketika menjalankan -a dan anak program -baru

## Langkah Pengerjaan
1. Buat fungsi createfolder serta mengzipnya
2. Buat fungsi removefolder
3. Buat fungsi killer

## lukisan.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>

void remove_folder(char *foldername)
{
    pid_t pid = fork();
    if (pid == -1)
    {
        perror("Failed to fork");
        exit(1);
    }
    else if (pid == 0)
    {
        // child process
        char command[100];
        sprintf(command, "rm -rf %s", foldername);
        execlp("sh", "sh", "-c", command, NULL);
        exit(0);
    }
}

void zip_folder(char *foldername)
{
    pid_t pid = fork();
    if (pid == -1)
    {
        perror("Failed to fork");
        exit(1);
    }
    else if (pid == 0)
    {
        // child process
        char command[100];
        sprintf(command, "zip -rq %s.zip %s", foldername, foldername);
        execlp("sh", "sh", "-c", command, NULL);
        exit(0);
    }
    else if (pid > 0)
    {
        int zip_status;
        waitpid(pid, &zip_status, 0);
        remove_folder(foldername);
    }
}

void create_killer(char *program_name, int child_pid, int mode)
{
    FILE *fp;
    char killer_name[50];
    sprintf(killer_name, "killer");

    fp = fopen(killer_name, "w+");

    if (mode)
        fprintf(fp, "#!/bin/bash\nkill -9 %d\nrm $0\n", child_pid);
    else
    {
        fprintf(fp, "#!/bin/bash\n pkill -f \"%s -b\"\nrm $0\n", program_name);
    }

    fclose(fp);

    chmod(killer_name, 0777);
}

int main(int argc, char *argv[])
{

    pid_t pid,
        sid; // Variabel untuk menyimpan PID

    pid = fork(); // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
     * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
     * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    // close(STDIN_FILENO);
    // close(STDOUT_FILENO);
    // close(STDERR_FILENO);
    int i, j;
    int delay_folder = 30;  // penundaan pembuatan folder dalam detik
    int delay_download = 5; // penundaan download gambar dalam detik

    if (argc >= 2 && strcmp(argv[1], "-a"))
        create_killer(argv[0], (int)getpid(), 0);
    else
    {
        create_killer(argv[0], (int)getpid(), 1);
    }
    while (1)
    {
        pid_t child_id = fork();
        if (child_id == 0)
        {
            // download gambar
            char foldername[50];

            struct tm *timeinfo;
            time_t now = time(NULL);
            timeinfo = localtime(&now);
            strftime(foldername, 50, "%Y-%m-%d_%H:%M:%S", timeinfo);
            mkdir(foldername, 0777);

            for (j = 0; j < 15; j++)
            {
                char filename[50];
                struct tm *rawtime;
                time_t ftime = time(NULL);
                struct tm *timefile = localtime(&ftime);
                strftime(filename, 50, "%Y-%m-%d_%H:%M:%S", timefile);
                strcat(filename, ".jpg");

                // download gambar dari https://picsum.photos/
                int epoch;
                epoch = (int)time(NULL);
                char url[50];
                sprintf(url, "https://picsum.photos/%d", (epoch % 950) + 50);
                char command[100];
                sprintf(command, "./%s/%s", foldername, filename);

                pid_t pid;
                pid = fork();
                if (pid == -1)
                {
                    perror("Failed to fork");
                    exit(1);
                }
                else if (pid == 0)
                {
                    // child process
                    execlp("wget", "wget", "-O", command, "-q", url, NULL);
                    exit(0);
                }

                // tunggu 5 detik sebelum download gambar berikutnya
                sleep(delay_download);
            }
            zip_folder(foldername);
            exit(1);
        }
        sleep(delay_folder);
    }

    return 0;
}
```

## Test Output

program lukisan -a
![O1](https://i.ibb.co/9Nw59S5/lukisan-a.png)

program lukisan -b
![O1](https://i.ibb.co/Dr8HRY2/lukisan-b.png)

## kendala
saat membuat program killer sedikit bingung karena sering terjadi error.


# Nomor 3 
## Analisa Soal
Pertama-tama sistem mengunduh file pada link ‣ dan dikumpulkan di zip bernama palyers.zip. Setelah itu file pada zip dipindahkan ke dalam folder dan hapuslah zip tersebut. Lalu, hapus file yang bukan Manchester United. Lalu buatlah 4 folder dengan kategori bek, gelandang, kiper, dan penyerang lalu masukkan file-file tersebut sesuai dengan kategori. Lalu, buat sistem mempunyai 11 file dengan rating tertinggi dengan inputan bek, gelandang, dan penyerang sesuai dengan yang diinginkan. Output yang keluar dimasukkan kedalam file bernama Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt

## Langkah Pengerjaan
1. Buatlah file progam dengan `nano filter.c` dan masukkan code nya
2. Periksa apakah code dapat berjalan dengan `gcc filter.c -o filter`
3. Setelah itu run dengan `./filter`

## filter.c
Untuk mendownload file dari drive, memasukkan ke player.zip lalu di extract ke folder dan hapus zip
```
pid = fork();
    if (pid == 0) {
         execlp("wget", "wget","-O","players.zip","https://drive.google.com/uc?export=download&id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF", NULL);         exit(EXIT_SUCCESS);
     } else {
         waitpid(pid, &status, 0);
     }

    pid = fork();
    if (pid == 0) {
        execlp("unzip", "unzip","-oq", "players.zip","-d","players", NULL);
        exit(EXIT_SUCCESS);
    } else {
        waitpid(pid, &status, 0);
    }

    pid = fork();
    if (pid == 0) {
        execlp("rm", "rm", "players.zip", NULL);
        exit(EXIT_SUCCESS);
    } else {
        waitpid(pid, &status, 0);
    }
```
Untuk menghapus file yang bukan Manchester United
```
pid = fork();
if (pid == -1) {
    printf("Gagal membuat proses baru untuk menghapus file yang tidak mengandung 'ManUtd'.\n");
    return 1;
} else if (pid == 0) {
    if (execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "!", "-name", "*ManUtd*", "-exec", "rm", "{}", "+", NULL) == -1) {
        printf("Gagal menjalankan perintah find.\n");
        return 1;
    }
} else {
    waitpid(pid, &status, 0);
    if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
        printf("File yang tidak mengandung 'ManUtd' dihapus.\n");
    else
        printf("Gagal menghapus file yang tidak mengandung 'ManUtd'.\n");
}
```
Fungsi untuk membuat folder dan mengsortir file sesuai dengan peran
```
pid = fork();
if (pid == 0) {
    execlp("mkdir", "mkdir", "./players/players/Penyerang", NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}

pid = fork();
if (pid == 0) {
    execlp("find", "find", "./players/players", "-maxdepth", "1", "-type", "f", "-name", "*Penyerang*", "-exec", "mv", "-t", "./players/players/Penyerang", "{}", "+", (char *) NULL);
    perror("execlp");
    exit(EXIT_FAILURE);
} else if (pid < 0) {
    perror("fork");
    exit(EXIT_FAILURE);
} else {
    waitpid(pid, &status, 0);
}
```
Mengsortir untuk menemukan kesebelasan terbaik dan memasukkan ke file
```
for (int i = 0; i < num_pemain && num_selected_kiper < num_kiper; i++) {
        if (strcmp(pemain[i].posisi, "Kiper") == 0) {
            printf(" %s (ranking: %d) posisi kiper \n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_kiper++;
        }
    }
    for (int i = 0; i < num_pemain && num_selected_bek < num_bek; i++) {
        if (strcmp(pemain[i].posisi, "Bek") == 0) {
            printf(" %s (ranking: %d) posisi bek\n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_bek++;
        }
    }
    for (int i = 0; i < num_pemain && num_selected_gelandang < num_gelandang; i++) {
        if (strcmp(pemain[i].posisi, "Gelandang") == 0) {
            printf(" %s (ranking: %d) posisi gelandang\n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_gelandang++;
        }
    }

    for (int i = 0; i < num_pemain && num_selected_penyerang < num_penyerang; i++) {
        if (strcmp(pemain[i].posisi, "Penyerang")== 0) {
            printf("%s (ranking: %d) posisi penyerang\n", pemain[i].name, pemain[i].ranking);
            selectedPlayer[selected_count] = pemain[i];
            selected_count++;
            num_selected_penyerang++;
        }
    }
char filename[100];
sprintf(filename, "./users/formasi_%d_%d_%d.txt", num_bek, num_gelandang, num_penyerang);
FILE *file = fopen(filename, "w");
fprintf(file, "Formasi terbaik untuk %d-%d-%d:\n", num_bek, num_gelandang, num_penyerang);
fprintf(file, "-----------------------\n");
for (int i = 0; i < 11; i++) {
    fprintf(file, " %s Posisi: %s, name: %s, ranking: %d\n",
        "\n",selectedPlayer[i].posisi , selectedPlayer[i].name, selectedPlayer[i].ranking);
}
fclose(file);}
```
## Test Output

Sudah terdapat folder players

![O1](https://i.ibb.co/wcds02d/l.png)

Sudah terdapat folder dengan 4 kategori 

![O1](https://i.ibb.co/Qm6Hspq/ll.png)

Output setelah progam di run 

![O1](https://i.ibb.co/4Ns48pS/lll.png)

Output pada file txt

![O1](https://i.ibb.co/bdYM3BR/llll.png)

## Kendala

Terdapat kendala saat memasukkan output ke dalam file txt

# Nomor 4
## Analisa Soal
Membuat sebuah program scheduling seperti cron job, dengan input jam, menit, detik dan file.sh
Diminita menampilkan pesan error jika tidak menerima argumen yang benar
Program berjalan di background dan bonus jika cpu state minimum

## Langkah Pengerjaan
Membuat program yang menerima argumen sesuai perintah, dan menampilkan semua error jika terdapat argumen yang tidak valid
Mengecek waktu setiap detik
Karena dilarang menggunakan system, digunakan exec dengan fork
Untuk berjalan di background, menggunakan daemon

## mainan.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>

void create_daemon() {
    pid_t pid = fork();

    if (pid < 0) exit(1);

    if (pid > 0) exit(0);

    umask(0);

    pid_t sid = setsid();

    if (sid < 0) exit(1);

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
}

void run_script(char* script_path) {
    pid_t pid = fork();

    if (pid == 0) {
        int fd = open("/dev/null", O_RDWR);
        dup2(fd, STDIN_FILENO);
        dup2(fd, STDOUT_FILENO);
        dup2(fd, STDERR_FILENO);
        close(fd);
        
        execl("/bin/bash", "bash", script_path, NULL);
        exit(1);
    } else if (pid > 0) {
        wait(NULL);
    } else {
        exit(1);
    }
}

int is_time_valid(int hour, int minute, int second) {
    time_t current_time = time(NULL);
    struct tm *local_time = localtime(&current_time);

    return ((hour == -1 || local_time->tm_hour == hour) &&
        (minute == -1 || local_time->tm_min == minute) &&
        (second == -1 || local_time->tm_sec == second));
}

void run_loop(int hour, int minute, int second, char* script_path) {
    while (1) {
        if (is_time_valid(hour, minute, second)) {
            run_script(script_path);
        }
        sleep(1);
    }
}

int main(int argc, char *argv[]) {
    int err;
    if (argc != 5) {
        printf("Penggunaan: %s <Jam> <Menit> <Detik> <Path_file>\n", argv[0]);
        return 1;
    }

    int hour = (strcmp(argv[1], "*") == 0) ? -1 : atoi(argv[1]);
    int minute = (strcmp(argv[2], "*") == 0) ? -1 : atoi(argv[2]);
    int second = (strcmp(argv[3], "*") == 0) ? -1 : atoi(argv[3]);
    char *script_path = argv[4];

    if (hour < -1 || hour > 23) {
        printf("Error: Jam harus diantara 0-23 atau tanda *\n");
        err = 1;
    }
    if (minute < -1 || minute > 59) {
        printf("Error: Menit harus diantara 0-59 atau tanda *\n");
        err = 1;
    }
    if (second < -1 || second > 59) {
        printf("Error: Detik harus diantara 0-59 atau tanda *\n");
        err = 1;
    }
    if (access(script_path, F_OK) == -1) {
    printf("Error: File tidak ditemukan\n");
    err = 1;
    } else {
        // cek apakah file adalah file bash
        char *extension = strrchr(script_path, '.');
        if (extension == NULL || strcmp(extension, ".sh") != 0) {
            printf("Error: File harus berformat .sh\n");
            err = 1;
        }
    }

    if(err) return 1;

    create_daemon();

    run_loop(hour, minute, second, script_path);

    return 0;
}
```
## Test Output
Error di suatu argumen (cth: file bukan sh)
![error1](https://i.ibb.co/FnnPsW0/err1.jpg)

Error di banyak argumen (cth: jam dan menit)
![error2](https://i.ibb.co/6Fm2P2W/err2.jpg)

CPU state
![cpu](https://i.ibb.co/k466Q0n/4-cpu.jpg)

## Kendala
kesulitan untuk membuat berjalan di background

##Revisi
Terdapat revisi untuk menambahkan pesan error pada argumen path file.sh
