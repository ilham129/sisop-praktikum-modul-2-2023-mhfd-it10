#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <dirent.h>
#include <string.h>

void download_file() {
    pid_t pid = fork();

    if (pid == 0) { // child process
        char *args[] = {"wget", "-q", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        execvp("wget", args);
    } 
    else { // parent process
        wait(NULL);
        printf("Download selesai.\n");
    }
}

void unzip_file() {
    pid_t pid2 = fork();

    if (pid2 == 0) { // child process
        char *args[] = {"unzip", "-q", "binatang.zip", "-d", ".", NULL};
        execvp("unzip", args);
    } 
    else { // parent process
        wait(NULL);
        printf("Unzip selesai.\n");
    }
}

void pick_random() {
    DIR *dir;
    struct dirent *ent;
    char *folder = ".";
    int count = 0;
    char *files[100];

    dir = opendir(folder);

    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            char *filename = ent->d_name;
            if (strcmp(filename + strlen(filename) - 4, ".jpg") == 0) {
                files[count] = filename;
                count++;
            }
        }
    }

    closedir(dir);

    srand(time(NULL));
    int shuffle = rand() % count;

    printf("Shift Jaga: %s\n", files[shuffle]);
}

void group_files() {
    char *folders[] = {"HewanDarat", "HewanAir", "HewanAmphibi"};
    for (int i = 0; i < 3; i++) mkdir(folders[i], 0777);
    
    DIR *dir;
    struct dirent *ent;
    char *folder = ".";
    
    dir = opendir(folder);
    
    while ((ent = readdir(dir)) != NULL) {
        if (ent->d_type == DT_REG) {
            char *filename = ent->d_name;
            char* file_extension = strrchr(filename, '.');
            if (file_extension && strcmp(file_extension, ".jpg") == 0) {
                char* animal_type = strrchr(filename, '_');
                char* dir_path;
                if (strcmp(animal_type + 1, "darat.jpg") == 0) {
                    dir_path = "HewanDarat";
                } 
                else if (strcmp(animal_type + 1, "amphibi.jpg") == 0) {
                    dir_path = "HewanAmphibi";
                } 
                else {
                    dir_path = "HewanAir";
                }
                char path[1000];
                sprintf(path, "%s/%s", dir_path, filename);
                rename(filename, path);
            }
        }
    }

    closedir(dir);
}

void zip_files() {
    int status;
    char *folders[] = {"HewanDarat", "HewanAir", "HewanAmphibi"};
    for (int i = 0; i < 3; i++) {
        pid_t pid = fork();

        if (pid == 0) {
            // Child process:
            char *args[] = {"zip", "-qr", folders[i], folders[i], NULL};
            execvp("zip", args);
        } else if (pid > 0) {
            // Parent process
            waitpid(pid, &status, 0);
        } else {
            // Error forking
            printf("Error forking\n");
            exit(1);
        }
    }
    printf("Zip selesai.\n");
    // Wait for all child processes to finish
    while (wait(&status) > 0);

    // Remove folders
    for (int i = 0; i < 3; i++) {
        pid_t pid = fork();

        if (pid == 0) {
            char* rm_args[] = {"rm", "-rf", folders[i], NULL};
            execvp("rm", rm_args);

            // If execvp() returns, there was an error
            perror("Error executing rm command");
            exit(EXIT_FAILURE);
        } 
        else if (pid < 0) {
            // Fork error
            perror("Error forking");
            exit(EXIT_FAILURE);
        }
    }

    // Wait for all child processes to finish
    while (wait(&status) > 0);
}

int main() {
    //A
    download_file();
    unzip_file();
    //B
    pick_random();
    //C
    group_files();
    //D
    zip_files();
    return 0;
}

